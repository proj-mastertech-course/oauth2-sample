package br.com.mastertech.sample.oauth;

import br.com.mastertech.sample.oauth.models.Usuario;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

public class UsuarioExtractor implements PrincipalExtractor {

    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        int id = (Integer) map.get("id");
        String name = (String) map.get("name");
        return new Usuario(id, name);
    }
}
