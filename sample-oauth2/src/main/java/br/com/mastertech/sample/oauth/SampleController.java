package br.com.mastertech.sample.oauth;

import br.com.mastertech.sample.oauth.models.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @GetMapping("/get")
    public String controller(@AuthenticationPrincipal Usuario usuario) {
        return usuario.toString();
    }
}
