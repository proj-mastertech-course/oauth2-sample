package br.com.mastertech.sample.oauth.models;

public class Usuario {

    private final int id;
    private final String name;

    public Usuario(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("Usuario: %d, %s", id, name);
    }
}
